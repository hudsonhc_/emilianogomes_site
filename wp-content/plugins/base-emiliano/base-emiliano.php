<?php

/**
 * Plugin Name: Base Emiliano.
 * Description: Controle base do tema Emiliano.
 * Version: 0.1
 * Author: Agência Palupa
 * Author URI: http://www.palupa.com.br
 * Licence: GPL2
 */


function baseEmiliano () {

		// TIPOS DE CONTEÚDO
		conteudosEmiliano();

		// TAXONOMIA
		taxonomiaEmiliano();

		// META BOXES
		metaboxesEmiliano();

		// SHORTCODES
		// shortcodesEmiliano();

	    // ATALHOS VISUAL COMPOSER
	    //visualcomposerEmiliano();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosEmiliano (){

		// TIPOS DE CONTEÚDO

		tipoDestaque();

		tipoBiografia();

		tipoAlbum();

		tipoVideo();

		tipoProjeto();

		tipoEnquete();

		tipoNoticia();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Nome do Destaque';
				break;

				case 'biografia':
					$titulo = 'Título do Item de Biografia';
				break;

				case 'album':
					$titulo = 'Nome do Álbum';
				break;

				case 'video':
					$titulo = 'Título do Vídeo';
				break;

				case 'projeto':
					$titulo = 'Título do Projeto';
				break;

				case 'enquete':
					$titulo = 'Pergunta da Enquete';
				break;

				case 'noticia':
					$titulo = 'Título da Notícia';
				break;

				default:
				break;
			}

		    return $titulo;
		}

	}

		// CUSTOM POST TYPE DESTAQUE
		function tipoDestaque() {

			$rotulosDestaque= array(
									'name'               => 'Destaque',
									'singular_name'      => 'Destaque',
									'menu_name'          => 'Destaque',
									'name_admin_bar'     => 'Destaque',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Destaque',
									'new_item'           => 'novo Destaque',
									'edit_item'          => 'Editar Destaque',
									'view_item'          => 'Ver Destaque',
									'all_items'          => 'Todos os Destaque',
									'search_items'       => 'Buscar Destaque',
									'parent_item_colon'  => 'Do Destaque',
									'not_found'          => 'Nenhum Destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum Destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE BIOGRAFIA
		function tipoBiografia() {

			$rotulosBiografia= array(
									'name'               => 'Biografia',
									'singular_name'      => 'Biografia',
									'menu_name'          => 'Biografia',
									'name_admin_bar'     => 'Biografia',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova Biografia',
									'new_item'           => 'nova Biografia',
									'edit_item'          => 'Editar Biografia',
									'view_item'          => 'Ver Biografia',
									'all_items'          => 'Todas as Biografias',
									'search_items'       => 'Buscar Biografias',
									'parent_item_colon'  => 'Da Biografia',
									'not_found'          => 'Nenhuma Biografia cadastrada.',
									'not_found_in_trash' => 'Nenhuma Biografia na lixeira.'
								);

			$argsBiografia 	= array(
									'labels'             => $rotulosBiografia,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 5,
									'menu_icon'          => 'dashicons-id',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'biografia' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('biografia', $argsBiografia);

		}

		// CUSTOM POST TYPE ÁLBUM
		function tipoAlbum() {

			$rotulosAlbum= array(
									'name'               => 'Álbuns',
									'singular_name'      => 'Álbuns',
									'menu_name'          => 'Álbuns',
									'name_admin_bar'     => 'Álbuns',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Álbum',
									'new_item'           => 'novo Álbum',
									'edit_item'          => 'Editar Álbum',
									'view_item'          => 'Ver Álbum',
									'all_items'          => 'Todos os Álbuns',
									'search_items'       => 'Buscar Álbum',
									'parent_item_colon'  => 'Do Álbum',
									'not_found'          => 'Nenhum Álbum cadastrado.',
									'not_found_in_trash' => 'Nenhum Álbum na lixeira.'
								);

			$argsAlbum 	= array(
									'labels'             => $rotulosAlbum,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 6,
									'menu_icon'          => 'dashicons-camera',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'album' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('album', $argsAlbum);

		}

		// CUSTOM POST TYPE VIDEO
		function tipoVideo() {

			$rotulosVideo= array(
									'name'               => 'Vídeos',
									'singular_name'      => 'Vídeos',
									'menu_name'          => 'Vídeos',
									'name_admin_bar'     => 'Vídeos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Vídeo',
									'new_item'           => 'novo Vídeo',
									'edit_item'          => 'Editar Vídeo',
									'view_item'          => 'Ver Vídeo',
									'all_items'          => 'Todos os Vídeos',
									'search_items'       => 'Buscar Vídeos',
									'parent_item_colon'  => 'Do Vídeo',
									'not_found'          => 'Nenhum Vídeo cadastrado.',
									'not_found_in_trash' => 'Nenhum Vídeo na lixeira.'
								);

			$argsVideo 	= array(
									'labels'             => $rotulosVideo,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 7,
									'menu_icon'          => 'dashicons-format-video',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'video' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('video', $argsVideo);

		}

		// CUSTOM POST TYPE PROJETO
		function tipoProjeto() {

			$rotulosProjeto= array(
									'name'               => 'Projetos',
									'singular_name'      => 'Projetos',
									'menu_name'          => 'Projetos',
									'name_admin_bar'     => 'Projetos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Projeto',
									'new_item'           => 'novo Projeto',
									'edit_item'          => 'Editar Projeto',
									'view_item'          => 'Ver Projeto',
									'all_items'          => 'Todos os Projetos',
									'search_items'       => 'Buscar Projetos',
									'parent_item_colon'  => 'Do Projeto',
									'not_found'          => 'Nenhum Projeto cadastrado.',
									'not_found_in_trash' => 'Nenhum Projeto na lixeira.'
								);

			$argsProjeto 	= array(
									'labels'             => $rotulosProjeto,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 8,
									'menu_icon'          => 'dashicons-portfolio',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'projeto' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('projeto', $argsProjeto);

		}

		// CUSTOM POST TYPE ENQUETE
		function tipoEnquete() {

			$rotulosEnquete= array(
									'name'               => 'Enquetes',
									'singular_name'      => 'Enquetes',
									'menu_name'          => 'Parlamento Dig.',
									'name_admin_bar'     => 'Parlamento Dig.',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova Enquete',
									'new_item'           => 'nova Enquete',
									'edit_item'          => 'Editar Enquete',
									'view_item'          => 'Ver Enquete',
									'all_items'          => 'Todas as Enquetes',
									'search_items'       => 'Buscar Enquetes',
									'parent_item_colon'  => 'Da Enquete',
									'not_found'          => 'Nenhuma Enquete cadastrada.',
									'not_found_in_trash' => 'Nenhuma Enquete na lixeira.'
								);

			$argsEnquete 	= array(
									'labels'             => $rotulosEnquete,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 8,
									'menu_icon'          => 'dashicons-thumbs-up',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'enquete' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('enquete', $argsEnquete);

		}

		// CUSTOM POST TYPE ENQUETE
		function tipoNoticia() {

			$rotulosNoticia= array(
									'name'               => 'Notícias',
									'singular_name'      => 'Notícias',
									'menu_name'          => 'Notícias',
									'name_admin_bar'     => 'Notícias',
									'add_new'            => 'Adicionar nova',
									'add_new_item'       => 'Adicionar nova Notícia',
									'new_item'           => 'nova Notícia',
									'edit_item'          => 'Editar Notícia',
									'view_item'          => 'Ver Notícia',
									'all_items'          => 'Todas as Notícias',
									'search_items'       => 'Buscar Notícias',
									'parent_item_colon'  => 'Da Notícia',
									'not_found'          => 'Nenhuma Notícia cadastrada.',
									'not_found_in_trash' => 'Nenhuma Notícia na lixeira.'
								);

			$argsNoticia 	= array(
									'labels'             => $rotulosNoticia,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 3,
									'menu_icon'          => 'dashicons-testimonial',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'noticia' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor', 'comments' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('noticia', $argsNoticia);

		}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaEmiliano () {

		taxonomiaCategoriaNoticia();

	}

		function taxonomiaCategoriaNoticia() {

			$rotulosCategoriaNoticia = array(
												'name'              => 'Categorias da Noticia',
												'singular_name'     => 'Categoria da Noticia',
												'search_items'      => 'Buscar categorias da Noticia',
												'all_items'         => 'Todas categorias da Noticia',
												'parent_item'       => 'Categoria da Noticia pai',
												'parent_item_colon' => 'Categoria da Noticia pai:',
												'edit_item'         => 'Editar categoria da Noticia',
												'update_item'       => 'Atualizar categoria da Noticia',
												'add_new_item'      => 'Nova categoria da Noticia',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias da Noticia',
											);

			$argsCategoriaNoticia 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosCategoriaNoticia,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-noticia' ),
											);

			register_taxonomy( 'categoriaNoticia', array( 'noticia' ), $argsCategoriaNoticia );

		}


    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesEmiliano(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Emiliano_';

			// METABOXES DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'destaqueMetabox',
				'title'			=> 'Detalhes do destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
                        'name'  => 'Link do destaque:',
                        'id'    => "{$prefix}destaque_link",
                        'desc'  => '',
                        'type'  => 'text'
                    ),


					array(
                        'name'  => 'Id do vídeo destaque:',
                        'id'    => "{$prefix}destaque_linkVideo",
                        'desc'  => '',
                        'type'  => 'text'
                    ),

                    array(
			 			'name'     => 'Tipo do destaque',
			 			'id'       => "{$prefix}destaque_tipo",
			 			'type'     => 'select',
			 			'options'  => array(
											'texto' => __( 'Imagem e Texto'),
											'video' => __( 'Vídeo' ),
										),
			 		),
				),
				'validation' 	=> array()
			);

			// METABOXES DE ÁLBUNS
			$metaboxes[] = array(

				'id'			=> 'albumMetabox',
				'title'			=> 'Galeria de Fotos',
				'pages' 		=> array( 'album' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
                        'name'  			=> 'Data do Álbum:',
                        'id'    			=> "{$prefix}album_data",
                        'desc'  			=> '',
                        'type'  			=> 'date',
                        'js_options'  		=> array(
                    								'dateFormat' =>	'dd-mm-yy'
                    							)
                    ),

                     array(
                        'name'  			=> 'Carregar Fotos:',
                        'id'    			=> "{$prefix}album_galeria",
                        'desc'  			=> '',
                        'type'  			=> 'image_advanced',
                        // 'max_file_uploads'  => 9
                    )

				),
				'validation' 	=> array()
			);

			// METABOXES DE BIOGRAFIA
			$metaboxes[] = array(

				'id'			=> 'biografiaMetabox',
				'title'			=> 'Detalhes: ',
				'pages' 		=> array( 'biografia' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					// array(
     //                    'name'  			=> 'Ano:',
     //                    'id'    			=> "{$prefix}bio_ano",
     //                    'desc'  			=> '',
     //                    'type'  			=> 'text',
     //                    'size'				=> 4,
     //                ),

                    array(
                        'name'  			=> 'Descrição:',
                        'id'    			=> "{$prefix}bio_info",
                        'desc'  			=> '',
                        'type'  			=> 'text',
                        'size'				=> 50 ,
                    )
				),
				'validation' => array(
				    'rules'    => array(
				        "{$prefix}bio_ano" => array(
				            'maxlength' => 4,
				            'minlength' => 4
				        ),
				         "{$prefix}bio_ano" => array(
				            'maxlength' => 80
				        )
				    ),
				    'messages' => array(
				        "{$prefix}bio_ano" => array(
				            'maxlength' => __( 'O campo ano deve ter exatamente 4 números.', 'your-prefix' ),
				            'minlength' => __( 'O campo ano deve ter exatamente 4 números.', 'your-prefix' )
				        ),
				         "{$prefix}bio_info" => array(
				            'maxlength' => __( 'A descrição deve ser menor.', 'your-prefix' ),
				        )
				    )
				)
			);

			// METABOXES DE VÍDEOS
			$metaboxes[] = array(

				'id'			=> 'videoMetabox',
				'title'			=> 'Detalhes do Vídeo',
				'pages' 		=> array( 'video' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
                        'name'  => 'Link do youtube:',
                        'id'    => "{$prefix}video_link",
                        'desc'  => '',
                        'type'  => 'text'
                    ),

                    array(
						'name'  => 'Breve Descrição do Vídeo: ',
						'id'    => "{$prefix}video_resumo",
						'desc'  => '',
						'type'  => 'text'
					),
				),
				'validation' => array(
				    'rules'    => array(
				        "{$prefix}video_resumo" => array(
				            'maxlength' => 300
				        )
				    ),
				    'messages' => array(
				        "{$prefix}video_resumo" => array(
				            'maxlength' => __( 'A sua descrição precisa ser menor.', 'your-prefix' ),
				        )
				    )
				)
			);

			// METABOXES DE PROJETO
			$metaboxes[] = array(

				'id'			=> 'projetoMetabox',
				'title'			=> 'Detalhes do Projeto',
				'pages' 		=> array( 'projeto' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
                        'name'  			=> 'Arquivo:',
                        'id'    			=> "{$prefix}projeto_arquivo",
                        'desc'  			=> '',
                        'type'  			=> 'file',
                        'max_file_uploads'  => 1
                    ),

                    array(
                        'name'  			=> 'Data do Projeto:',
                        'id'    			=> "{$prefix}projeto_data",
                        'desc'  			=> '',
                        'type'  			=> 'date',
                        'js_options'  		=> array(
                    								'dateFormat' =>	'dd-mm-yy'
                    							)
                    )
				),
				'validation' 	=> array()
			);

			// METABOXES DE ENQUETE
			$metaboxes[] = array(

				'id'			=> 'enqueteMetabox',
				'title'			=> 'Detalhes da Enquete',
				'pages' 		=> array( 'enquete' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
                        'name'  			=> 'Foto de fundo:',
                        'id'    			=> "{$prefix}enquete_fundo",
                        'desc'  			=> '',
                        'type'  			=> 'image_advanced',
                        'max_file_uploads'  => 1
                    ),

                    array(
                        'name'  			=> 'Ícone do enquete:',
                        'id'    			=> "{$prefix}enquete_icone",
                        'desc'  			=> '',
                        'type'  			=> 'image_advanced',
                        'max_file_uploads'  => 1
                    ),

                    array(
                        'name'  			=> 'Cor do enquete:',
                        'id'    			=> "{$prefix}enquete_cor",
                        'desc'  			=> '',
                        'type'  			=> 'color'
                    ),

                    array(
                        'name'  			=> 'Shortcode da Enquete:',
                        'id'    			=> "{$prefix}enquete_code",
                        'desc'  			=> '',
                        'type'  			=> 'text'
                    )
				),
				'validation' 	=> array()
			);

			return $metaboxes;
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesEmiliano(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerEmiliano(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseEmiliano');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	//add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseEmiliano();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );