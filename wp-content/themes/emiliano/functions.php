<?php
/**
 * emiliano functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package emiliano
 */

if ( ! function_exists( 'emiliano_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function emiliano_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on emiliano, use a find and replace
	 * to change 'emiliano' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'emiliano', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'emiliano' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'emiliano_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'emiliano_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function emiliano_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'emiliano_content_width', 640 );
}
add_action( 'after_setup_theme', 'emiliano_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function emiliano_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'emiliano' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'emiliano_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function emiliano_scripts() {

	wp_enqueue_script( 'emiliano-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'emiliano-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	//FONT GOOGLE
	wp_enqueue_style( 'emiliano-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');

	//JAVA SCRIPT
	wp_enqueue_script( 'emiliano-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' );
	wp_enqueue_script( 'emiliano-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'emiliano-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'emiliano-bootsrap', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( 'emiliano-font-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js' );
	wp_enqueue_script( 'emiliano-jquery-facybox', get_template_directory_uri() . '/fancybox/source/jquery.fancybox.pack.js' );
	wp_enqueue_script( 'emiliano-jquery-facybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-buttons.js' );
	wp_enqueue_script( 'emiliano-jquery-facybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-media.js' );
	wp_enqueue_script( 'emiliano-jquery-facybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-thumbs.js' );
	// wp_enqueue_script( 'emiliano-word-split', get_template_directory_uri() . '/js/jquery.lettering.js' );
	wp_enqueue_script( 'emiliano-geral', get_template_directory_uri() . '/js/geral.js' );

	//CSS
	wp_enqueue_style( 'emiliano-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style( 'emiliano-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style( 'emiliano-font-animate', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style( 'emiliano-font-owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style( 'emiliano-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/jquery.fancybox.css');
	wp_enqueue_style( 'emiliano-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-buttons.css');
	wp_enqueue_style( 'emiliano-jquery-fancybox', get_template_directory_uri() . '/fancybox/source/helpers/jquery.fancybox-thumbs.css');
	wp_enqueue_style( 'emiliano-style', get_template_directory_uri() . '/css/site.css');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'emiliano_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// CONFGURAÇÕES VIA REDUX
if (class_exists('ReduxFramework')) {
	require_once (get_template_directory() . '/redux/sample-config.php');
}

/************************************************
*	REMOVER O EDITOR DE CONTEUDO DAS PÁGINAS	*
*************************************************/
add_action('init', 'remove_content_editor');

function remove_content_editor() {
    remove_post_type_support( 'video', 'editor' );
    remove_post_type_support( 'biografia', 'editor' );
    remove_post_type_support( 'projeto', 'editor' );
}

/****************************************************
*	DEFINIR LIMITE DO RESUMO DO CONTEÚDO DO POST	*
*****************************************************/
function resume($excerpt, $limit) {
	$resume = explode(' ', $excerpt, $limit);
	while (count($resume)>=$limit) {
	array_pop($resume);
	$resume = implode(" ",$resume).'...';
	}
	$resume = preg_replace('`\[[^\]]*\]`','',$resume);
	return $resume;
}

function novo_tamanho_do_resumo($length) {
	return 30;
}
add_filter('excerpt_length', 'novo_tamanho_do_resumo');

// EXTENSÃO DA FUNÇÂO WP_GET_ARCHIVES
function extensaoFiltrosArchive($where,$args){

	$year		= isset($args['year']) 		? $args['year'] 	: '';
	$month		= isset($args['month']) 	? $args['month'] 	: '';
	$monthname	= isset($args['monthname']) ? $args['monthname']: '';
	$day		= isset($args['day']) 		? $args['day'] 		: '';
	$dayname	= isset($args['dayname']) 	? $args['dayname'] 	: '';
	if($year){
		$where .= " AND YEAR(post_date) = '$year' ";
		$where .= $month ? " AND MONTH(post_date) = '$month' " : '';
		$where .= $day ? " AND DAY(post_date) = '$day' " : '';
	}
	if($month){
		$where .= " AND MONTH(post_date) = '$month' ";
		$where .= $day ? " AND DAY(post_date) = '$day' " : '';
	}
	if($monthname){
		$where .= " AND MONTHNAME(post_date) = '$monthname' ";
		$where .= $day ? " AND DAY(post_date) = '$day' " : '';
	}
	if($day){
		$where .= " AND DAY(post_date) = '$day' ";
	}
	if($dayname){
		$where .= " AND DAYNAME(post_date) = '$dayname' ";
	}
	return $where;
}

add_filter( 'getarchives_where', 'extensaoFiltrosArchive',10,2);

//PAGINAÇÃO
function pagination($pages = '', $range = 4){
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class=\"paginador\">";
         //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         //if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

 		$htmlPaginas = "";
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 $htmlPaginas .= ($paged == $i)? '<li ><a href="' . get_pagenum_link($i) . '">' . $i . '</a></li>' : '<li ><a href="' . get_pagenum_link($i) . '" >' . $i . '</a></li>';
             }
         }

         if ($paged < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($paged + 1) . '"><i class="fa fa-angle-double-left"></i></a>';
         echo $htmlPaginas;
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo '<a href="' . get_pagenum_link($pages) . '"><i class="fa fa-angle-double-right"></i></a>';
         echo "</div>\n";
     }
}

// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_styles($styles)
{
$styles->default_version = "01022019";
}
add_action("wp_default_styles", "my_wp_default_styles");

 	// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_scripts($scripts)
{
$scripts->default_version = "01022019";
}
add_action("wp_default_scripts", "my_wp_default_scripts");
