<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emiliano
 */
global $configuracao;
?>
<!-- RODAPÉ -->
	<footer>
		<div class="container">
			<div class="col-md-9 rights text-center">© Copyright 2016 - Vereador Emiliano Gomes - Todos os direitos reservados</div>
			<div class="col-md-3 social text-center">
				<ul>
					<li><a href="<?php echo $configuracao['opt-instagram'] ?>" target="_blank"><i class="fa fa-instagram"></a></i></li>
					<li><a href="<?php echo $configuracao['opt-youtube'] ?>" target="_blank"><i class="fa fa-youtube"></a></i></li>
					<li><a href="<?php echo $configuracao['opt-twitter'] ?>" target="_blank"><i class="fa fa-twitter"></a></i></li>
					<li><a href="<?php echo $configuracao['opt-facebook'] ?>" target="_blank"><i class="fa fa-facebook"></a></i></li>
				</ul>
			</div>
		</div>
	</footer>
<?php wp_footer(); ?>

</body>
</html>
