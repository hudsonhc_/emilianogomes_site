<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emiliano
 */
global $configuracao;
get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-perfilFrase']; ?></p>
	</div>
</div>

<!-- ARCHIVE ALBUNS -->
<div class="pg pg-album">

	<div class="container">
		<section class="conteudo">
			<div class="row">
				<div class="titulo">Nossos Álbuns<i class="fa fa-camera-retro"></i></div>
			</div>
			<div class="row">
				<div class="subtitulo"><?php echo $configuracao['opt-albuns-textoDescricao']; ?></div>
			</div>

			<div class="row">
				<?php
					while ( have_posts() ) : the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];
						$data = rwmb_meta('Emiliano_album_data');
				?>

				<div class="col-sm-12 col-md-6 coluna">
					<a href="<?php echo get_permalink(); ?>" title="Clique e veja" alt="Clique e veja" class="foto" style=" background: url(<?php echo $foto;?>) no-repeat;">
						<div class="lente">
							<!-- <a href="<?php echo get_permalink(); ?>" class="botao"><p>ver álbum</p> <i class="fa fa-angle-double-right"></i></a>-->
						</div> 
					</a>
					<div class="data"><?php echo $data; ?></div>
					<div class="resumo"><?php echo the_title(); ?></div>

				</div>

				<?php endwhile; wp_reset_query();  ?>
			</div>

			<!-- PAGINAÇÃO -->
			<div class="paginador">
				<?php if (function_exists("pagination")) {
					    pagination();
					}
				?>
			</div>
		</section>
	</div>
</div>


<?php get_footer(); ?>
