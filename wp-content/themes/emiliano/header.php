<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emiliano
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-67616006-9', 'auto');
	  ga('send', 'pageview');
	</script>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />

<?php
	if (has_post_thumbnail()){
	$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
	$foto = $foto[0];
 ?>

<meta property="og:title" content="<?php echo get_the_title() ?>" />
<meta property="og:description" content="<?php echo $textoresumido = get_the_excerpt() ?> "/>
<meta property="og:url" content="" />
<meta property="og:image" content="<?php echo $foto ?>"/>
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<?php }?>
</head>

<body>
	<?php
		global $configuracao;
	?>
	<!-- TOPO -->
	<div class="area-topo">

			<header class="topo">
				<nav class="navbar" role="navigation">

					<!-- MENU MOBILE TRIGGER -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<div class="row navbar-header">
					 <div class="faixa"></div>
						<!-- LOGOTIPO / MENU MOBILE-->
						<div class="container">

							<!-- LOGO -->
							<div class="logo">
								<a href="<?php echo home_url('/index.php/'); ?>" title="Emiliano Gomes - Vereador">
									<h1>
										Emiliano Gomes - Vereador
									</h1>
									<img src="<?php bloginfo('template_directory'); ?>/img/logotipo.png" alt="">
								</a>
							</div>


							<!-- MENU -->
							<div class="menu">
								<nav class="collapse navbar-collapse" id="collapse">
									<ul class="nav navbar-nav">
										<li><a href="<?php echo home_url('/index.php/'); ?>" class="cmn-t-underline">Início</a></li>
										<li class="sub-menu-dropdown">
											<a href="#">Blog</a>
											<ul class="sub-menu">
												<li class="sub-menu-item"><a href="<?php echo home_url('/blog/'); ?>" class="cmn-t-underline">Blog</a></li>
												<li class="sub-menu-item"><a href="<?php echo home_url('/noticia/'); ?>" class="cmn-t-underline">Notícias</a></li>
											</ul>
										</li>
										<li><a href="<?php echo home_url('/biografia/'); ?>" class="cmn-t-underline">Biografia</a></li>
										<li class="sub-menu-dropdown">
											<a href="#">Mídias</a>
											<ul class="sub-menu">

												<li class="sub-menu-item"><a href="<?php echo home_url('/album/'); ?>" class="cmn-t-underline">Fotos</a></li>
												<li class="sub-menu-item"><a href="<?php echo home_url('/video/'); ?>" class="cmn-t-underline">Vídeos</a></li>

											</ul>
										</li>



										


										<li class="sub-menu-dropdown">
											<a href="#">Fenix Co-Working</a>
											<ul class="sub-menu">
												<li class="sub-menu-item"><a href="<?php echo home_url('/quem-somos/'); ?>" class="cmn-t-underline">Quem somos</a></li>

												<!-- <li class="sub-menu-item"><a href="<?php echo home_url('/parlamento/'); ?>" class="cmn-t-underline">Qual sua opnião?</a></li> -->
												


												<li class="sub-menu-item menuProjetos">
													<a href="<?php echo home_url('/projeto/'); ?>" class="cmn-t-underline ">Projetos</a>
													<ul class="sub-menu">
														<li class="sub-menu-item"><a href="<?php echo home_url('/cidade-solucoes/'); ?>" class="cmn-t-underline">Cidades & Soluções</a></li>
														<li class="sub-menu-item"><a href="<?php echo home_url('/conexao-saude/'); ?>" class="cmn-t-underline">Conexão Saúde</a></li>
														<li class="sub-menu-item"><a href="<?php echo home_url('/sankalpa-yoga-meditacao/'); ?>" class="cmn-t-underline">Sankalpa Yoga & Meditação</a></li>
														<li class="sub-menu-item"><a href="<?php echo home_url('/mega-busca/'); ?>" class="cmn-t-underline">Mega Busca</a></li>
														<li class="sub-menu-item"><a href="<?php echo home_url('/kohpan/'); ?>" class="cmn-t-underline">Kohpan</a></li>
													</ul>

												</li>

												
											
											
											</ul>
										</li>
										

										<li><a href="<?php echo home_url('/contato/'); ?>" class="cmn-t-underline">Contato</a></li>

									</ul>
								</nav>
							</div>

						</div>

					</div>
				</nav>
			</header>

	</div>
