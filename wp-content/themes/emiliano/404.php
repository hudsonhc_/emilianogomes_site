<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package emiliano
 */

get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p>" <b>O descontentamento</b> é o primeiro passo na evolução de um homem ou de uma <b>nação</b> "<br/><span>Emiliano Gomes</span></p>
	</div>
</div>

<div class="pg pg-404">
	<div class="container">
		<div class="titulo">Oops! Essa página não foi encontrada.</div>
		<div class="texto">Deseja voltar à <a href="<?php echo home_url('/index.php/'); ?>">Página Inicial</a>?</div>
	</div>
</div>

<?php
get_footer();?>
