<?php 
/**
*	Template Name: Padrão
*	Template Description: Template padrão para criarção de novas páginas
*
*	@package emiliano
*/	
get_header();
?>

	<div class="pg-padrao">
		<div class="topo-internas">
			<div class="lente">
				<p>" <b>O descontentamento</b> é o primeiro passo na evolução de um homem ou de uma <b>nação</b> "<br/><span>Emiliano Gomes</span></p>
			</div>
		</div>
		<div class="container">
			<?php echo the_content(); ?>
		</div>
	</div>
<?php get_footer(); ?>