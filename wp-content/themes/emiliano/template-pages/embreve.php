<?php
/**
 * Template Name: Em Breve
 * Description: 
 *
 * @package Emiliano
 */
get_header(); ?>

<body>
	
	<!-- CONTEÚDO DO SITE -->
	<div class="pg pg-inicial" style="display: none;"></div>

	<!-- PÁGINA EM BREVE -->
	<div class="pg pg-embreve" style="display: ;">

			<?php if ( have_posts() ) : while( have_posts() ) : the_post();

				$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$foto = $foto[0];

			endwhile; endif; ?>		
			
			<!-- CABEÇALHO DA PÁGINA -->
			<section class="topo" style=" background: url('<?php echo $foto; ?>') no-repeat;">
				<div class="container">
					<div class="elementos-topo">
						<div class="logo">
							<img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt="">
							<p><strong>Em breve<br/> Vereador</strong> Emiliano Gomes</p>
						</div>
						<div class="col-lg-1 sociais">
							<a href=""><i class="fa fa-facebook"></i></a>
							<a href=""><i class="fa fa-twitter"></i></a>
							<a href=""><i class="fa fa-instagram"></i></a>
							<a href=""><i class="fa fa-youtube"></i></a>
						</div>
						<div class="frase">
							<div class="col-sm-12 texto text-center">"<strong>O descontentamento</strong> é o primeiro passo na evolução de um homem ou de uma <strong>nação</strong>"</div>
							<div class="col-sm-12 nome text-right">Emiliano Gomes</div>
						</div>
					</div>						
				</div>					
			</section>

			<!-- FORMULÁRIO DE CONTATO -->
			<section class="contato">
				<div class="container">
					<div class="col-md-5">
						<div class="paragrafo1">
							Fale com <strong>Emiliano Gomes</strong> 
							<i class="fa fa-arrow-circle-o-right"></i>
						</div>
						<div class="paragrafo2">
							<strong>Preencha o formulário</strong>, Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum repudiandae voluptatibus dolore voluptates ab consequuntur cupiditate aliquid totam et repellendus minima fugit tempore, iure, in magni deserunt qui deleniti quidem.
						</div>
					</div>
					<div class="col-md-7">
						<div class="formulario">							
							<?php
	                           echo do_shortcode('[contact-form-7 id="18" title="Formulário Em Breve"]');
	                        ?>
						</div>
					</div>
				</div>					
			</section>

			<!-- GALERIA DE IMAGENS -->
			<div class="container">
				<section class="galeria">
					<?php 

                        // while ( have_posts() ) : the_post(); 

                            $_paginafotos = rwmb_meta( 'Emiliano_embreve_galeria', 'type=image' );

                            $paginafotos = array();
                            foreach ($_paginafotos as $foto) {
                            	$paginafotos[] = $foto;
                            }
                	?>
					<div class="row">						  
						<div class="col-sm-3 img-responsive foto" style=" background: url(<?php echo $paginafotos[0]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[0]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
						<div class="col-sm-5 img-responsive foto" style=" background: url(<?php echo $paginafotos[1]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[1]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
						<div class="col-sm-3 img-responsive foto" style=" background: url(<?php echo $paginafotos[2]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[2]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-5 img-responsive foto" style=" background: url(<?php echo $paginafotos[3]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[3]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
						<div class="col-sm-3 img-responsive foto" style=" background: url(<?php echo $paginafotos[4]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[4]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
						<div class="col-sm-3 img-responsive foto" style=" background: url(<?php echo $paginafotos[5]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[5]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 img-responsive foto" style=" background: url(<?php echo $paginafotos[6]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[6]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
						<div class="col-sm-5 img-responsive foto" style=" background: url(<?php echo $paginafotos[7]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[7]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
						<div class="col-sm-3 img-responsive foto" style=" background: url(<?php echo $paginafotos[8]['full_url']; ?>) no-repeat;">
							<div class="lente"></div>
							<div class="lente-verde">
								<a id="example1" rel="gallery1" href="<?php echo $paginafotos[8]['full_url']; ?>"><i class="fa fa-search-plus"></i></a>
							</div>
						</div>
					</div>

					<?php  
					// endwhile; ?>				

				</section>
			</div>			

			<section class="copyright">
				<div class="container">
					<div class="conteudo">
						<div class="col-lg-9 texto text-center">© Copyright 2016 - Vereador Emiliano Gomes - Todos os direitos reservados</div>
						<div class="col-lg-3 sociais text-right">
							<a href=""><i class="fa fa-facebook"></a></i>
							<a href=""><i class="fa fa-twitter"></a></i>
							<a href=""><i class="fa fa-instagram"></a></i>
							<a href=""><i class="fa fa-youtube"></a></i>
						</div>
					</div>
				</div>						
			</section>
		
	</div>


</body>