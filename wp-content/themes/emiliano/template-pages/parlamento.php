<?php
/**
 * Template Name: Parlamento
 * Description: 
 *
 * @package Emiliano
 */
global $configuracao;
get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-perfilFrase']; ?></p>
	</div>
</div>

<div class="pg pg-parlamento" style="display: ;">		

	<div class="container">
		<section class="conteudo">			
			<div class="titulo">Parlamento Digital<i class="fa fa-hand-pointer-o"></i></div>
			<div class="subtitulo"><?php echo $configuracao['opt-parlamento-textoDescricao']; ?></div>

			<div class="area-parlamento">

				<!-- LOOP PROJETOS  -->
				<?php 
					$enquetes = new WP_Query( array( 'post_type' => 'enquete', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );
			  		
					while ( $enquetes->have_posts() ) : $enquetes->the_post();
						$foto 	= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto 	= $foto[0];
						$fundo 	= array_shift(rwmb_meta('Emiliano_enquete_fundo', 'type=image'));
						$icone 	= array_shift(rwmb_meta('Emiliano_enquete_icone', 'type=image'));						
						$cor 	= rwmb_meta('Emiliano_enquete_cor');
						$code 	= rwmb_meta('Emiliano_enquete_code');
				?>

				<div class="row projs">
					<div class="col-md-3 pesquisa">
						<img src="<?php echo $icone['full_url']; ?>" alt="">
						<div class="shortcode">
							<?php
	                       		echo do_shortcode(''.$code.'');
	                    	?>
	                    </div>
					</div>
					<div class="col-md-9 fundo" style=" background-color: <?php echo $cor; ?>;">
						<div class="row lente" style=" background: url(<?php echo $fundo['full_url']; ?>) no-repeat;"></div>
						<div class="row conteudo3">
							<div class="col-md-7 resumo">
								<div class="titulo-enquete" style=" color: <?php echo $cor; ?>;"><?php echo the_title(); ?></div>
								<p><?php echo the_content(); ?></p>
							</div>
							<div class="col-md-5 foto"><img src="<?php echo $foto; ?>" alt="" class="img-responsive"></div>
						</div>
					</div>
				</div>

				<?php endwhile; wp_reset_query();  ?>
				
			</div>

		</section>
	</div>

</div>

<?php get_footer(); ?>	