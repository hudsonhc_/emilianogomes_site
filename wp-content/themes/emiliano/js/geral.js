
	$(function(){


		/*****************************************
		*  	 CARROSSEL PÁGINA INCIAL DESTAQUE
		*****************************************/
  		$("#carrossel-inicial").owlCarousel({

		 	items : 1,
	        dots: true,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag: false,
	        autoplay:false,
		    animateOut: 'fadeOut',
			// itemsDesktop : [1199,3],
			// itemsDesktopSmall : [979,3]
  		});

  	
		$('#video_player').trigger('play');
		$('#video_player')[0].play();
		$('#video_player').click(function(){
				 $('#video_player')[0].webkitEnterFullScreen();
   				 $('#video_player')[0].play();
		 });
  
  		/*********************************
		*  	 CARROSSEL CANAL YOUTUBE
		*********************************/
		$('#carrossel-youtube').owlCarousel({
			items: 4,
			autoplay: true,
			loop: true,
			dots:false,
			responsive: {
				0: {
					items: 1
				},
				500: {
					items: 2
				},
				800: {
					items: 3
				},
				1000: {
					items: 4
				}
			}
		});
		var carrosselYoutube = $("#carrossel-youtube").data('owlCarousel');

		$('.navegacaoT').click(function(){ carrosselYoutube.prev(); });
		$('.navegacaoF').click(function(){ carrosselYoutube.next(); });


		$("a#example1").fancybox({
			'titleShow'     : false,
			openEffect	: 'elastic',
    		closeEffect	: 'elastic',
    		closeBtn    : true,
    		arrows      : true,
    		nextClick   : true
		});

		$(".foto2").mouseenter(function(){
		    $(this).find('.lente').hide();
		    $(this).find('.lente-verde').show("slide");
		});

		$(".foto2").mouseleave(function(){
		    $(".foto .lente").show();
		    $(this).find('.lente-verde').hide();
		});

		$(".foto").mouseenter(function(){
		    $(this).find('.lente').show();
		});

		$(".foto").mouseleave(function(){
		    $(".foto .lente").hide();
		});

	});