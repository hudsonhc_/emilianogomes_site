<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package emiliano
 */

get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-perfilFrase']; ?></p>
	</div>
</div>

<div class="pg pg-fotos">		
	
	<div class="container">
		<section class="conteudo2">
			<div class="titulo">Fotos <i class="fa fa-camera-retro"></i></div>
			<div class="data"><?php echo rwmb_meta('Emiliano_album_data'); ?>
				<p> <?php echo the_title(); ?></p>
			</div>
			<div class="row fotos">

				<?php
					$fotos = rwmb_meta( 'Emiliano_album_galeria', 'type=image' );
					$paginafotos = array();	                            
                    	foreach ( $fotos as $foto ):
				?>

				<div class="col-sm-6 col-md-3 coluna">

					<a  id="fancy" rel="gallery1" href="<?php echo $foto['full_url']?>" class="foto"  style=" background: url(<?php echo $foto['full_url']?>) no-repeat;">
						<div class="lente"></div>
					</a>
				</div>
			
				<?php 	endforeach; ?>

			</div>
			<a href="<?php echo home_url('/album/'); ?>" class="botao"><i class="fa fa-angle-double-left"></i><p> voltar</p></a>
		</section>
	</div>		
</div>
<script>
		$("a#fancy").fancybox({			
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true			 
	});
</script>
<?php get_footer(); ?>