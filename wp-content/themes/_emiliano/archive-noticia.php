<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emiliano
 */
global $configuracao;
get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-perfilFrase']; ?></p>
	</div>
</div>

<div class="pg pg-blog">
	<div class="container">
		<section class="blog">
			<div class="img-fundo" style=" background: url(<?php bloginfo('template_directory'); ?>/img/fundo-blog.png) no-repeat;">

				<!-- MENU LATERAL -->
				<div class="row posts">

					<div class="row">
						<div class="col-xs-12 titulo">Últimas Notícias<i class="fa fa-newspaper-o"></i></div>
					</div>

					<div class="row">
						<div class="col-md-3">

							<!-- <div class="panel-group" id="accordion" aria-multiselectable="true">

								<?php
									/* Seleciona os anos no banco de dados */
									// $anos = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'noticia' ORDER BY post_date DESC");
									// $i = 0;
									// foreach($anos as $ano) :
								?>
								<div class="panel">
									<div class="panel-heading" id="headingTwo">
										<div class="panel-title">
											<a class="collapsed botao" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?php echo $i; ?>" aria-expanded="false" aria-controls="collapseTwo">Arquivos <?php echo $ano; ?> <i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
									<div id="collapseTwo<?php echo $i; ?>" class="panel-collapse collapse" aria-labelledby="headingTwo">
										<div class="panel-body">
											<ul>
												<?php

													// $args = array(
													// 				'post_type' => 'noticia',
													// 				'type' => 'monthly',
													// 				'echo' => 0,
													// 				'year' => ''.$ano.'',
													// 				);


													// echo wp_get_archives( $args );
												?>
											</ul>
										</div>
									</div>
								</div>

								<?php  //$i++; endforeach; ?>

							</div> -->

							<?php
							$taxonomia = 'categoriaNoticia';
							$conteudoTaxonomias = get_terms( $taxonomia, '' );
							foreach ($conteudoTaxonomias as $conteudoTaxonomia):
								$nomeTaxonomia  = $conteudoTaxonomia->name;
								$termoTaxonomia = get_term($conteudoTaxonomia->term_id, $taxonomia);
								$linkTaxonomia  = get_term_link($termoTaxonomia);
							?>
							<a href="<?php echo $linkTaxonomia; ?>" class="blogCategoriaLink"><?php echo $nomeTaxonomia; ?></a>
							<?php endforeach;?>

						</div>

						<div class="col-md-9 blog">

								<div class="row sub-texto"><p><?php echo $configuracao['opt-noticias-textoDescricao']; ?></p></div>

								<div id="blog-container" class="blog-container">

									<ul class="blog-grid">
									<?php if ( have_posts() ) :

										while ( have_posts() ) : the_post();
					                        $foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					                        $foto = $foto[0];
					                        $categories = get_the_terms( $post, 'categoriaNoticia' );
					                        $categories_arr = array();
					                        foreach ($categories as $category)
											{
											    array_push($categories_arr, ' <a href="' . get_term_link($category) . '">#' . $category->name . '</a> ');
											}
				                     ?>

										<!-- BLOG -->
										<li>

											<a href="<?php echo get_permalink(); ?>" title="">
												<div class="col-md-6">

													<div class="imagem" style="background: url(<?php echo $foto;?>);">
														<div class="data">
															<div class="row">
																<div class="col-xs-12 data-texto">
																	<span class="dia"><?php the_time('d') ?></span>
																	<span class="mes"><?php the_time('F') ?></span>
																	<span class="ano"><?php the_time('Y') ?></span>
																	<span><i class="fa fa-calendar"></i></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row descricao">
														<div class="col-xs-12">
															<div class="categorias-post">
																<?= implode(", ", $categories_arr); ?>
															</div>
														</div>
														<div class="col-xs-12">
															<h2><?php echo get_the_title(); ?></h2>
														</div>
														<div class="col-xs-12">
															<div class="col-xs-12"><a href="<?php echo get_permalink(); ?>" class="botao"><p>ver mais </p><i class="fa fa-angle-double-right"></i></a></div>
														</div>
													</div>

												</div>
											</a>

										</li>

										<?php endwhile; ?>

					                <?php endif; ?>

									</ul>

								</div>
						</div>
					</div>

					<!-- PAGINAÇÃO -->
					<div class="paginador">

						<?php if (function_exists("pagination")) {
							    pagination();
							}
						?>
					</div>



				</div>
			</div>

		</section>
	</div>

</div>
<?php get_footer(); ?>
