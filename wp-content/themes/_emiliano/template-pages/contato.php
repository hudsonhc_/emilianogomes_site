<?php
/**
 * Template Name: Contato
 * Description: 
 *
 * @package Emiliano
 */
global $configuracao;
get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-perfilFrase']; ?></p>
	</div>
</div>

<div class="pg pg-contato">		

	<div class="container">	
		<section class="fale-conosco">		

			<div class="row">
				<div class="titulo">Fale com o Emiliano<i class="fa fa-envelope"></i></div>
				<div class="subtitulo"><?php echo $configuracao['opt-contato-textoDescricao']; ?></div>
			</div>
			
			<div class="row contato">
				<div class="col-md-5 info">
					<div class="paragrafo2">
						<a href="<?php echo home_url('/parlamento/'); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/parlamento-cinza.png" alt=""></a>
						<span><strong>Parlamento</strong> digital (projetos)</span>
						<p><?php echo $configuracao['opt-parlamento-textoChamada']; ?></p>
					</div>
				</div>
				<div class="col-md-7">
					<div class="formulario">
						
						<?php
	                       echo do_shortcode('[contact-form-7 id="18" title="Formulário Em Breve"]');
	                    ?>
	                    
					</div>
				</div>
			</div>

		</section>			
	</div>

</div>

<?php get_footer(); ?>	