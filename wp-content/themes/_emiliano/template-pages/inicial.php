<?php
/**
 * Template Name: Inicial
 * Description:
 *
 * @package Emiliano
 */
global $configuracao;
get_header();
$audio = $configuracao['opt-audioFundo']; ?>

<div class="audioplayer">
	<audio preload="auto" controls id="audio">
		<source src="<?php echo $audio; ?>" />
	</audio>
</div>
<div class="row areaPlayPause">
	<div class="col-md-12 text-right" id="sidebar-musica">
		<span class="txtTocar"><i class="fa fa-music tocarIcone"></i></span>
		<div class="play-pause"></div>

	</div>
</div>

<!-- CARROSSEL FUNDO eled-->
<div class="carrossel">

	<div id="carrossel-inicial" class="owl-Carousel">

		<!-- LOOP DESTAUQES  -->
		<?php
			$destaquesPost = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1 ) );

			while ( $destaquesPost->have_posts() ) : $destaquesPost->the_post();
				$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$foto = $foto[0];
				$link 	= rwmb_meta('Emiliano_destaque_link');
				$linkVideo = rwmb_meta('Emiliano_destaque_linkVideo');
				$tipoDestaque = rwmb_meta('Emiliano_destaque_tipo');
				$class = "";
		?>

		<!-- ITEM -->
		<?php
			if ($tipoDestaque == "video") {
				$class = "video";
				$foto = "";
			}
		?>
		<div class="item <?php echo $class ?>" style="background: url(<?php echo "$foto" ?>) no-repeat;">
			
			
				<?php
					if ($tipoDestaque == "video"):
				?>
				<div class="areaVideoDestaque">				

					<video class="areaVideoDestaque" autobuffer  style="width:100%;margin-top: 75px;" id="video_player" src="<?php echo $linkVideo ?>" loop autoplay preload="auto" muted></video>
						
				</div>
				<!-- <iframe width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe> -->
				<?php else: ?>
			<div class="container text-center">
				<div class="texto-carrossel">
					<span><b><?php echo the_title(); ?></b><?php echo the_content(); ?> </span>
					<a href="<?php echo $link; ?>" target="_blank" class="botao"><p>saiba mais </p><i class="fa fa-angle-double-right"></i></a>
				</div>
				
			</div>
			<?php endif; ?>
		</div>

		<?php endwhile; wp_reset_query(); ?>

	</div>
</div>

<div class="pg pg-inicial">

	<!-- PERFIL REDUX-->
	<section class="perfil">
		<div class="fundo">
			<div class="container">

				<!-- ESPAÇAMENTO PARA A FOTO -->
				<div class="col-md-3 fantasma"></div>

				<!-- TEXTO -->
				<div class="col-md-7 resumo">
					<p><?php echo $configuracao['opt-perfilInfo']; ?></p>
				</div>

				<!-- BOTÃO + -->
				<div class="link">
					<a href="<?php echo home_url('/biografia/'); ?>"><i class="fa fa-plus"></i></a>
				</div>

			</div>
		</div>
		<div class="container">

			<!-- FOTO -->
			<div class="col-md-3 foto">
				<img src="<?php echo $configuracao['opt-perfilFoto']['url']; ?>" alt="foto-perfil">
			</div>

		</div>
	</section>

	<!-- ÚLTIMAS NOTÍCIAS -->
	<section class="ultimas-noticias">
		<div class="container">

			<div class="col-md-12 titulo">Últimas notícias</div>

			<div class="row caixa">
				<div class="grid">

					<!-- LOOP NOTICIAS  -->
					<?php
						$args = array(
								  'post_type' => 'noticia',
								  'posts_per_page' => 3,
								  'caller_get_posts'=> 1
								);
						$news = null;
						$news = new WP_Query($args);

						while ( $news->have_posts() ) : $news->the_post();
							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];
					?>

					<figure class="effect-oscar">
						<img src="<?php echo $foto; ?>" alt="img-noticias"/>
						<figcaption>
							<h2><?php echo the_title(); ?></h2>
							<p><?php echo the_excerpt(); ?></p>
							<a href="<?php echo get_permalink(); ?>">View more</a>
						</figcaption>
					</figure>

					<?php endwhile; wp_reset_query();  ?>

				</div>
			</div>
		</div>
	</section>

	<!-- CARROSSEL DE IMAGENS -->
	<section class="row  galeria-carrossel">
		<div class="col-md-12 skewoff">
			<div class="container">

				<!-- TÍTULO -->
				<div class="titulo">
					<span>Fotos</span>
					<i class="fa fa-camera-retro"></i>
				</div>

				<!-- FOTOS -->
				<div class="galeria">
					<?php
						$args = array(
								  'post_type' => 'album',
								  'posts_per_page' => -1
								);
						$albuns = null;
						$albuns = new WP_Query($args);
						$i = 1;
                        while ( $albuns->have_posts() ) : $albuns->the_post();
                            $fotos = rwmb_meta( 'Emiliano_album_galeria', 'type=image' );
                            if($i >= 10){
								break;
							}
                            foreach ( $fotos as $foto ) {

                	?>

								<!-- ITEM -->
								<div class="item">
									<a id="fancy" rel="gallery1" href="<?php echo $foto['full_url']; ?>" >

										<!-- FOTO GALERIA -->
										<div class="img-galeria" style="background: url(<?php echo $foto['full_url']; ?>) "></div>

									</a>
								</div>

					<?php
								if($i >= 10){
									break;
								}$i++;

							}
						endwhile; wp_reset_query();
					?>
					<!-- BOTÃO -->
					<div class="col-xs-12"><a href="<?php echo home_url('/album/'); ?>" class="botao"><p>ver mais </p><i class="fa fa-angle-double-right"></i></a></div>

				</div>
			</div>
		</div>
	</section>

	<!-- ÚLTIMOS VÍDEOS -->
	<div class="container">
		<section class="videos">

			<!-- TÍTULO -->
			<div class="row titulo">
				<span>Vídeos</span>
				<i class="fa fa-youtube-play"></i>
			</div>

			<div class="row galeria">
				<?php
					$args = array(
							  'post_type' => 'video',
							  'posts_per_page' => 3,
							  'caller_get_posts'=> 1
							);
					$videos = null;
					$videos = new WP_Query($args);

                    while ( $videos->have_posts() ) : $videos->the_post();
                        $link 	= rwmb_meta('Emiliano_video_link');
						$link 	= str_replace("watch?v=", "embed/", $link);
            	?>
					<!-- ITEM -->
					<div class="col-md-4 col-sm-6 item-video">
						<!-- FOTO GALERIA -->
						<iframe width="300" height="300" src="<?php echo $link; ?>" frameborder="0" allowfullscreen></iframe>
					</div>

				<?php
					endwhile; wp_reset_query();
				?>

				<!-- BOTÃO -->
				<div class="col-xs-12"><a href="<?php echo home_url('/video/'); ?>" class="botao"><p>ver mais </p><i class="fa fa-angle-double-right"></i></a></div>

			</div>
		</section>
	</div>

	<!-- FORMULÁRIO DE CONTATO -->
	<section class="fale-conosco">
		<div class="container">
			<div class="titulo">Fale com <b>Emiliano Gomes</b><i class="fa fa-arrow-circle-o-right"></i></div>
		</div>

		<div class="contato">
			<div class="container">

				<!-- TEXTO E ICONES -->
				<div class="col-md-5">
					<div class="paragrafo1">
						<img src="<?php bloginfo('template_directory'); ?>/img/logo-formulario.png" alt="">
					</div>
					<div class="paragrafo2">
						<a href="<?php echo home_url('/parlamento/'); ?>"><img src="<?php bloginfo('template_directory'); ?>/img/parlamento-icone.png" alt=""></a>
						<span><strong>Parlamento</strong> digital (projetos)</span>
						<p><?php echo $configuracao['opt-parlamento-textoChamada']; ?></p>
					</div>
				</div>

				<!-- FORMULÁRIO -->
				<div class="col-md-7">
					<div class="formulario">
						<?php
                           echo do_shortcode('[contact-form-7 id="18" title="Formulário Em Breve"]');
                        ?>
					</div>
				</div>

			</div>
		</div>
	</section>

</div>


<?php get_footer(); ?>

<script type="text/javascript">
	function writeCookie(name,value,days) {
		    var date, expires;
		    if (days) {
		        date = new Date();
		        date.setTime(date.getTime()+(days*24*60*60*1000));
		        expires = "; expires=" + date.toGMTString();
		            }else{
		        expires = "";
		    }
		    document.cookie = name + "=" + value + expires + "; path=/";
		}

		function readCookie(name) {
		    var i, c, ca, nameEQ = name + "=";
		    ca = document.cookie.split(';');
		    for(i=0;i < ca.length;i++) {
		        c = ca[i];
		        while (c.charAt(0)==' ') {
		            c = c.substring(1,c.length);
		        }
		        if (c.indexOf(nameEQ) == 0) {
		            return c.substring(nameEQ.length,c.length);
		        }
		    }
		    return '';
		}

		if (!readCookie('MusicPause')) {
			var pause = 'toca' ;
			writeCookie('MusicPause', pause, 3);
			playMusic();
		} else{
			pause = readCookie('MusicPause');
			if (pause == 'toca'){
				playMusic();
			}else{
				$(".tocarIcone").css('visibility','hidden');
				$(".play-pause").addClass("paused");
				audio = document.getElementById('audio');
			}
		};

		function playMusic(){
			audio = document.getElementById('audio');
			audio.volume = 0.5;
			audio.play();
		};

		$(".play-pause").click(function(){
			audio = document.getElementById('audio');
			if (audio.paused) {
				audio.play();
				$(".tocarIcone").css('visibility','visible');
				$(".play-pause").removeClass("paused");
				writeCookie('MusicPause', 'toca', 3);
			} else {
				audio.pause();
				$(".tocarIcone").css('visibility','hidden');
				$(".play-pause").addClass("paused");
				writeCookie('MusicPause', 'pausa', 3);
				audio = document.getElementById('audio');
			}
		});


		$("a#fancy").fancybox({			
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true			 
	});

</script>