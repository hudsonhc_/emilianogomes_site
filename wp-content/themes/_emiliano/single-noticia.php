<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package emiliano
 */

get_header(); ?>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=240078979376313";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<?php 
	if (has_post_thumbnail()){
	$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
	$foto = $foto[0];
 ?>

<meta property="og:title" content="Emiliano Gomes - <?php echo get_the_title() ?>" />
<meta property="og:description" content="<?php echo $textoresumido = get_the_excerpt() ?> "/>
<meta property="og:url" content="" />
<meta property="og:image" content="<?php echo $foto ?>"/>
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<?php }?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-biografia-textoImagem']; ?></p>
	</div>
</div>

<div class="pg pg-postagem">
	<div class="container">
		<section class="blog">
			<div class="img-fundo" style=" background: url(<?php bloginfo('template_directory'); ?>/img/fundo-blog.png) no-repeat;">

				<div class="row posts">
					<!-- MENU LATERAL stetica -->
					<div class="row">
						<div class="col-xs-12 titulo">Últimas Notícias<i class="fa fa-newspaper-o"></i></div>				
					</div>	
					<div class="row">
						<div class="col-md-3">

							<div class="panel-group" id="accordion" aria-multiselectable="true">

								<?php
									/* Seleciona os anos no banco de dados */																														
									$anos = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'noticia' ORDER BY post_date DESC");
									$i = 0;
									foreach($anos as $ano) :
								?>

								<!-- OPÇÃO MENU -->
								<div class="panel">
									<div class="panel-heading" id="headingTwo">
										<div class="panel-title">
											<a class="collapsed botao" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?php echo $i; ?>" aria-expanded="false" aria-controls="collapseTwo">Arquivos <?php echo $ano; ?> <i class="fa fa-angle-double-right"></i></a>
										</div>
									</div>
									<div id="collapseTwo<?php echo $i; ?>" class="panel-collapse collapse" aria-labelledby="headingTwo">
										<div class="panel-body">
											<ul>

												<?php

													$args = array(
																	'post_type' => 'noticia',
																	'type' => 'monthly',
																	'echo' => 0,
																	'year' => ''.$ano.'',
																	);

													echo wp_get_archives( $args );

												?>

											</ul>
										</div>
									</div>
								</div>

								<?php  $i++; endforeach; ?>

							</div>
						</div>

						<!-- ÁREA DA POSTAGEM DO BLOG -->
						<div class="col-md-9 post">
							<div class="sub-texto"></div>
							<?php
								if ( have_posts() ) : while( have_posts() ) : the_post();
									$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
			                        $foto = $foto[0];
			                        $categories = get_the_terms( $post, 'categoriaNoticia' );
			                        $categories_arr = array();
			                        foreach ($categories as $category)
									{
									    array_push($categories_arr, ' <a href="' . get_term_link($category) . '">#' . $category->name . '</a> ');
									}

							?>
							<div class="imagem" style="background: url(<?php echo "$foto" ?>)">							
								<div class="data">							
									<div class="col-xs-12 data-texto">
										<span class="dia"><?php the_time('d') ?></span>
										<span class="mes"><?php the_time('F') ?></span>
										<span class="ano"><?php the_time('Y') ?></span>
										<span><i class="fa fa-calendar"></i></span>
									</div>
								</div>
							</div>
							<div class="row conteudo">
								<div class="col-xs-12">
									<div class="categorias-post">
										<?= implode(", ", $categories_arr); ?>														
									</div>
								</div>
								<div class="titulo-post"><?php echo the_title(); ?></div>
								<p><?php echo the_content(); ?></p>
								<div class="fb-like" data-href="https://www.facebook.com/Emiliano-Rocha-Gomes-327859600651347/?fref=ts" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>								
							</div>
							<div class="row comentario">
								<?php
									//If comments are open or we have at least one comment, load up the comment template.
									if ( comments_open() || get_comments_number() ) :
										comments_template();
									endif;
								?>

							</div>

							<!-- NAGEVAÇÃO ENTRE POSTAGENS -->
							<div class="prev-post">
								<span><?php previous_post('%','<i class="fa fa-angle-double-left"></i> Anterior ', 'no') ?></span>
								<span><?php next_post('%','Próximo <i class="fa fa-angle-double-right"></i>','no')?></span>
							</div>

							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<?php get_footer(); ?>
