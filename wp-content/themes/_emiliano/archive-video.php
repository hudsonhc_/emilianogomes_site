<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emiliano
 */

get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-perfilFrase']; ?></p>
	</div>
</div>

<div class="pg pg-videos">
	
	<div class="container">
		<section class="conteudo">			
			<div class="titulo">Últimos vídeos<i class="fa fa-play-circle"></i></div>
			<div class="subtitulo"><?php echo $configuracao['opt-videos-textoDescricao']; ?></div>
				<div class="row">
					<?php 	
						while ( have_posts() ) : the_post();
							$link 	= rwmb_meta('Emiliano_video_link');
							$link 	= str_replace("watch?v=", "embed/", $link);						
							$resumo = rwmb_meta('Emiliano_video_resumo');
					?>
				
					<div class="col-sm-12 col-md-6 coluna">
						<div class="foto"><iframe width="436" height="328" src="<?php echo $link ?>" frameborder="0" allowfullscreen></iframe></div>
						<div class="resumo"><?php echo $resumo; ?></div>
					</div>

					<?php endwhile; wp_reset_query();  ?>
				</div>
			<div class="row canal">
				<div class="col-xs-12">
					<img src="<?php bloginfo('template_directory'); ?>/img/canal.png" alt="">
					<div class="link">
						Mais vídeos
						<p>Acesse o canal do <a href="https://www.youtube.com/channel/UCull9TUlqLlTukFPRCWtssg">youtube</a></p>
					</div>
				</div>
			</div>
			<div class="row carrossel">
				<div class="owl-carousel" id="carrossel-youtube">
					<?php 
						$conteudoLink = file_get_contents('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0Fo8xWJbujWePHPn2DrULTFkCX5loZkw&channelId=UCull9TUlqLlTukFPRCWtssg&part=snippet,id&order=date&maxResults=20');

				        $array        = json_decode($conteudoLink, true);

			        	foreach ($array['items'] as $video) {			        		
		        	?>

		        	<div class="item">
		        		<a href="https://www.youtube.com/watch?v=<?php echo $video['id']['videoId']; ?>" target="_blank"><img src="<?php echo $video['snippet']['thumbnails']['medium']['url']; ?>" alt=""><i class="fa fa-youtube-play"></i></a>		        		
		        	</div>

		        	<?php 	
			       		}
					?>				
					 
				</div>

				<!-- BOTÕES DE NAVEGAÇÃO -->
				<div class="botoes">
					<button class="navegacaoT hidden-xs"><i class="fa fa-angle-left"></i></button>
					<button class="navegacaoF hidden-xs"><i class="fa fa-angle-right"></i></button>
				</div>
			</div>

		</section>
	</div>
</div>

<?php get_footer(); ?>
