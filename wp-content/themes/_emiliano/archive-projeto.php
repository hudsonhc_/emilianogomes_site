<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emiliano
 */
global $configuracao;
get_header(); ?>

<div class="topo-internas">
	<div class="lente">
		<p><?php echo $configuracao['opt-perfilFrase']; ?></p>
	</div>
</div>

<div class="pg pg-projetos">		

	<div class="container">
		<section class="conteudo">			
			<div class="titulo">Projetos de Lei<i class="fa fa-balance-scale"></i></div>
			<div class="subtitulo"><?php echo $configuracao['opt-projetos-textoDescricao']; ?></div>

			<div class="area-projetos">
				<?php 	
					while ( have_posts() ) : the_post();
						$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$foto = $foto[0];
						$data = rwmb_meta('Emiliano_projeto_data');
						$files = array_shift(rwmb_meta( 'Emiliano_projeto_arquivo', 'type=file'));
				?>
				
				<div class="row projeto">
					<div class="col-md-2 data text-center"><?php echo $data; ?></div>
					<div class="col-md-7 texto">
						<p><?php echo the_title(); ?></p>
					</div>
					<div class="col-md-3 text-center"><a href="<?php echo $files['url']?>" class="botao-ver">Ver projeto <i class="fa fa-plus"></i></a></div>
				</div>

				<?php endwhile; wp_reset_query();  ?>	

				<!-- PAGINAÇÃO -->
				<div class="paginador">

					<?php if (function_exists("pagination")) {
						    pagination();
						}
					?>
				</div>

			</div>
		</section>
	</div>
</div>
	
<?php get_footer(); ?>