<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package emiliano
 */

get_header(); ?>

<div class="pg pg-biografia">
	<div class="col-md-12 topo-biografia">
		<div class="row resumo-topo">
			<div class="container">
				<p><?php echo $configuracao['opt-biografia-textoImagem']; ?></p>
			</div>
		</div>
	</div>

	<div class="container">
		<section class="resumo">
			<div class="container">
				<p><?php echo $configuracao['opt-biografia-textoPrincipal']; ?></p>
			</div>
		</section>

		
		<section class="biografia">
			<div class="row fundo">	
				<div class="linha"></div>				
				<div class="linha-do-tempo">				

					<?php 
						$i = 1;
						 

						$biografiaPost = new WP_Query( array( 'post_type' => 'biografia', 'orderby' => 'date', 'order' => 'ASC', 'posts_per_page' => -1 ) );
		                
		                while ( $biografiaPost->have_posts() ) : $biografiaPost->the_post();
						
							$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$foto = $foto[0];
							$ano = rwmb_meta('Emiliano_bio_ano');							

							if ($i == 1){
					?>

					<div class="row">
						<div class="col-xs-12 inicio">
							<div class="etapa-ano">
								<div class="ano"><p><?php echo get_the_date('Y'); ?></p></div>
							</div>							
							<div class="etapa-descricao">
								<div class="titulo-etapa"><?php echo the_title(); ?></div>
								<div class="texto"><?php echo rwmb_meta('Emiliano_bio_info'); ?></div>
							</div>
						</div>
					</div>

					<?php 

							}else if ($i%2 == 0){
					?>
						
					<div class="row">						
						<div class="col-xs-12 etapa par text-right">
							<div class="etapa-ano">
								<div class="ano"></div>	
								<p><?php echo get_the_date('Y'); ?></p>															
							</div>	
							<div class="etapa-descricao">
								<img class="img-responsive" src="<?php echo $foto; ?>" alt="">
								<div class="titulo-etapa"><?php echo the_title(); ?></div>
								<div class="texto"><?php echo rwmb_meta('Emiliano_bio_info'); ?></div>
							</div>
						</div>
					</div>

					<?php 	}else{ ?>

					<div class="row">
						<div class="col-xs-12 etapa impar text-left">
							<div class="etapa-ano">
								<p><?php echo get_the_date('Y'); ?></p>	
								<div class="ano"></div>																							
							</div>	
							<div class="etapa-descricao">
								<img class="img-responsive" src="<?php echo $foto; ?>" alt="">
								<div class="titulo-etapa"><?php echo the_title(); ?></div>
								<div class="texto"><?php echo rwmb_meta('Emiliano_bio_info'); ?></div>
							</div>
						</div>
					</div>

					<?php  	} $i++; ?>
					<?php endwhile; wp_reset_query();  ?>
				</div>										
			</div>
		</section>
	</div>

</div>

	
<?php get_footer(); ?>
